#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
# 版权说明
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

# @Time  : 2020/12/24 15:44

# @Author : mayn

# @Project : ZyxFlaskadminSystem

# @FileName: tree_helper.py

# @Software: PyCharm

# 作者：小钟同学

# 著作权归作者所有

# 文件功能描述: 
"""


def tree(data, root, root_field, node_field):
    """
    解析list数据为树结构
    :param data:  被解析的数据
    :param root: 根节点值
    :param root_field: 根节点字段
    :param node_field: 节点字段
    :return: list
    """
    l = []
    for i in data:
        if i.get(root_field) == root:
            l.append(i)

    # i的内存ID 是一样的 所以不需要对list进行修改
    for i in data:
        node = i.get(node_field)
        children = []
        for j in data:
            parent = j.get(root_field)
            if node == parent:
                children.append(j)

        if children:
            i['children'] = children

        # if ['children']:

        # del i['children']

    return l


# def list_to_tree(data):
#     '''
#     利用对象内存共享生成嵌套结构
#     :param data:
#     :return:
#     '''
#     res = {}
#     for v in data:
#         # v["parent_id"] = v["parent_id"] if v["parent_id"] else 0
#         # 以id为key，存储当前元素数据
#         res.setdefault(v["id"], v).update(v)  # .update(v) 解决先添加parent_id，后添加id的情况。
#         # 这里默认的关联关系，v的内存地址是一致的，所以后续修改之后，关联的结构数据也会变化。
#         res.setdefault(v["parent_id"], {}).setdefault("children", []).append(res.get(v["id"], v))
#     return res[0]["children"]

def list_to_tree_wuxu(data):
    '''
       利用对象内存共享生成嵌套结构
       :param data:
       :return:
       '''
    res = {}
    for v in data:
        # v["parent_id"] = v["parent_id"] if v["parent_id"] else 0
        # 以id为key，存储当前元素数据
        res.setdefault(v["id"], v)
    for v in data:
        res.setdefault(v["parent_id"], {}).setdefault("children", []).append(v)
        # 这里默认的关联关系，v的内存地址是一致的，所以后续修改只后，关联的结构也会变化。
    # print(res)
    return res[0]["children"]


if __name__ == '__main__':
    test_data = [
        {'id': 1, 'title': 'GGG', 'parent_id': 0},
        {'id': 2, 'title': 'AAA', 'parent_id': 0},
        {'id': 4, 'title': 'CCC', 'parent_id': 1},
        {'id': 5, 'title': 'DDD', 'parent_id': 2},
        {'id': 6, 'title': 'EEE', 'parent_id': 3},
        {'id': 7, 'title': 'FFF', 'parent_id': 4},
        {'id': 3, 'title': 'BBB', 'parent_id': 1},
    ]
    print(tree(test_data, 0, 'parent_id', 'id'))

    data = [
        {'id': 10, 'parent_id': 8, 'name': "ACAB"},
        {'id': 9, 'parent_id': 8, 'name': "ACAA"},
        {'id': 8, 'parent_id': 7, 'name': "ACA"},
        {'id': 7, 'parent_id': 1, 'name': "AC"},
        {'id': 6, 'parent_id': 3, 'name': "ABC"},
        {'id': 5, 'parent_id': 3, 'name': "ABB"},
        {'id': 4, 'parent_id': 3, 'name': "ABA"},
        {'id': 3, 'parent_id': 1, 'name': "AB"},
        {'id': 2, 'parent_id': 0, 'name': "AA"},
        {'id': 1, 'parent_id': 0, 'name': "A"},
    ]

    from common.helper import json_helper

    print(json_helper.dict_to_json(list_to_tree_wuxu(data)))

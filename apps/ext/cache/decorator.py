#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     decorator
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/28
-------------------------------------------------
   修改描述-2021/7/28:   缓存的装饰器
-------------------------------------------------
"""
from functools import wraps
from typing import Callable, Optional, Type

from apps.ext.cache import FastAPICache
from apps.ext.cache.coder import Coder


def cache(
    expire: int = None,
    coder: Type[Coder] = None,
    key_builder: Callable = None,
    namespace: Optional[str] = "",
):
    """
    cache all function
    :param namespace:
    :param expire:
    :param coder:
    :param key_builder:
    :return:
    """

    def wrapper(func):
        @wraps(func)
        async def inner(*args, **kwargs):
            nonlocal coder
            nonlocal expire
            nonlocal key_builder
            copy_kwargs = kwargs.copy()
            # 对请求的参数的提取
            request = copy_kwargs.pop("request", None)
            # 对请求结果的响应的处理
            response = copy_kwargs.pop("response", None)
            # 如果前端的要求不走缓存的机制的话，则执行函数的调用
            if request and request.headers.get("Cache-Control") == "no-store":
                return await func(*args, **kwargs)

            # huoqu
            coder = coder or FastAPICache.get_coder()
            expire = expire or FastAPICache.get_expire()
            key_builder = key_builder or FastAPICache.get_key_builder()
            backend = FastAPICache.get_backend()
            # 缓存key
            cache_key = key_builder(func, namespace, request=request, response=response, args=args, kwargs=copy_kwargs)

            ttl, ret = await backend.get_with_ttl(cache_key)
            if not request:
                if ret is not None:
                    return coder.decode(ret)
                ret = await func(*args, **kwargs)
                await backend.set(cache_key, coder.encode(ret), expire or FastAPICache.get_expire())

                return ret

            if request.method != "GET":
                return await func(request, *args, **kwargs)
            if_none_match = request.headers.get("if-none-match")
            if ret is not None:
                if response:
                    response.headers["Cache-Control"] = f"max-age={ttl}"
                    etag = f"W/{hash(ret)}"
                    if if_none_match == etag:
                        response.status_code = 304
                        return response
                    response.headers["ETag"] = etag

                return coder.decode(ret)

            ret = await func(*args, **kwargs)

            await backend.set(cache_key, coder.encode(ret), expire or FastAPICache.get_expire())
            return ret

        return inner

    return wrapper
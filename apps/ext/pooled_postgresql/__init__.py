#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
Author = zyx
@Create_Time: 2021/10/6 21:46
@version: v1.0.0
@Contact: 308711822@qq.com
@File: __init__.py.py
@文件功能描述:------
"""
from fastapi import FastAPI
from playhouse.pool import PooledPostgresqlExtDatabase
from apps.config.pgdb_conf import pgconf
from contextlib import contextmanager
from apps.utils.singleton_helper import Singleton

@Singleton
class SyncPooledPostgresqlClient:

    def __init__(self, app: FastAPI = None):
        # 如果有APPC传入则直接的进行初始化的操作即可
        self.database = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app: FastAPI):
        self.app = app

        @app.on_event("startup")
        def startup_event():
            app.state.sync_redis = self.init_sync_pooled_postgresql()

        @app.on_event("shutdown")
        def shutdown_event():
            app.state.redis.close()
            app.state.redis.wait_closed()

    def init_config(self):
        self.init_sync_pooled_postgresql()

    def init_sync_pooled_postgresql(self):
        self.database = PooledPostgresqlExtDatabase(
            pgconf.DB_NAME,
            max_connections=pgconf.DB_MAX_CONNECTIONS,
            stale_timeout=pgconf.DB_STALE_TIMEOUT,  # 5 minutes.
            timeout=pgconf.DB_TIMEOUT,
            **{'user': pgconf.DB_USER, 'host': pgconf.DB_HOST,
               'password': pgconf.DB_PASS,
               'port': pgconf.DB_PROT})

        self.prefix = ''
        self.timeout = 0
        return self.database

    def GetSession(self):
        return self.database

    def GetSessionAtomic(self):
        return self.database.atomic()

    def session_scope_atomic(self):
        return self.GetSessionAtomic()

    @contextmanager
    def session_scope(self):
        session = self.GetSession()
        if session.is_closed():
            # session.connection_context()
            session.connect()
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

    def session_scope2(self):
        session = self.GetSession()
        with session.manual_commit():
            session.begin()  # Begin transaction explicitly.
            try:
                yield session
            except:
                session.rollback()  # Rollback -- an error occurred.
                raise
            else:
                try:
                    session.commit()  # Attempt to commit changes.
                except:
                    session.rollback()  # Error committing, rollback.
                    raise
            finally:
                session.close()


sync_pooled_postgresql_client = SyncPooledPostgresqlClient()

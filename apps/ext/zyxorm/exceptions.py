#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     exceptions
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/2
-------------------------------------------------
   修改描述-2021/7/2:         
-------------------------------------------------
"""
from __future__ import absolute_import, unicode_literals
import six
from .utils import to_text, to_binary


class BaseException(Exception):

    def __init__(self, errcode, errmsg):
        """
        :param errcode: Error code
        :param errmsg: Error message
        """
        self.errcode = errcode
        self.errmsg = errmsg

    def __str__(self):

        if six.PY2:
            return to_binary('{code}: {message}'.format(code=self.errcode, message=self.errmsg))
        else:
            return to_text('{code}: {message}'.format(code=self.errcode, message=self.errmsg))

    def __repr__(self):
        # 默认情况下，__repr__() 会返回和调用者有关的 “类名+object at+内存地址”信息
        # __repr__() 方法是类的实例化对象用来做“自我介绍”的方法
        return to_text(f'{self.__class__.__name__}({self.errcode}, {self.errmsg})')


class InvalidVerifyException(BaseException):

    def __init__(self, errcode=1000, errmsg='Invalid Verify Exception'):
        super().__init__(errcode, errmsg)


class ClientConfigBadException(BaseException):

    def __init__(self, errcode=20000, errmsg='还没进行数据库信息的配置'):
        super().__init__(errcode, errmsg)

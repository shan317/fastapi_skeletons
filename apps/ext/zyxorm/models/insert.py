#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     insert
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/12
-------------------------------------------------
   修改描述-2021/7/12:         
-------------------------------------------------
"""
from dataclasses import dataclass
from apps.ext.ormx.models.base import Base
from apps.ext.ormx.exceptions import ClientConfigBadException


@dataclass
class Insert(Base):
    pass

    def __columnize(self, columns):
        # 把字典类型的资源化为元祖类型，且元祖的内容元素使用“” 进行包裹处理！
        return tuple(columns).__str__().replace('\'', '"')

    def __valueize(self, data):
        print("data", data)
        # 把字典类型的值进行值使用（，）的方式进行包裹处理，取字典的原始的值，
        # 首先先把所有的值给添加到一个元祖列表里面
        # 然后列表进行串联
        return ','.join([tuple(index.values()).__str__() for index in data])

    def create(self, data):
        if data:
            # 如果是字典类型需要转换处理一下
            if isinstance(data, dict):
                data = [{key: value for key, value in data.items()}]
            # 如果是列表的类型则不需要处理
            self.__sql__ = self.__compile_create__(data)
        else:
            raise ClientConfigBadException(errmsg='请传入字典格式的数据！')
        return self

    def __compile_create__(self, data):
        if not self.select_table:
            raise ClientConfigBadException(errmsg='请设置需要查询查询的表')

        return "insert into {} {} values {}{}".format(self.select_table, self.__columnize(data[0]), self.__valueize(data), self._compile_returning())

    def __compile_insert(self, columns, data):
        if not self.select_table:
            raise ClientConfigBadException(errmsg='请设置需要查询查询的表')
        return "insert into {} {} values {}{}".format(self.select_table, self.__columnize(columns), ','.join([tuple(index).__str__() for index in data]), self._compile_returning())

    def insert(self, columns, data):
        self.__sql__ = self.__compile_insert(columns, data)
        return self
